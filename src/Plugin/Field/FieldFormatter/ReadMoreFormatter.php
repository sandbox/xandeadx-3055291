<?php

namespace Drupal\simple_read_more\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldFormatter(
 *   id = "simple_read_more",
 *   label = @Translation("Text with read-more link"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   },
 * )
 */
class ReadMoreFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'max_length' => 600,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Max length'),
      '#default_value' => $this->getSetting('max_length'),
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $formatter_settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      $text = $item->value;
      $break_pos = mb_strpos($text, '<!--break-->');

      if ($break_pos !== FALSE || mb_strlen($text) > $formatter_settings['max_length'] + 100) {
        $summary = text_summary($text, $item->format, $formatter_settings['max_length']);
        $more = mb_substr($text, $break_pos ? $break_pos + 12 : $formatter_settings['max_length']);
        $text = '
          <div class="collapsible-text">
            <div class="collapsible-text__summary">
              ' . $summary . '
            </div>
            <div class="collapsible-text__text">
              ' . $more . '
            </div>
            <a class="collapsible-text__link" href="#">Читать далее</a>
          </div>
        ';
      }

      $elements[$delta] = [
        '#type' => 'processed_text',
        '#text' => $text,
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
        '#attached' => [
          'library' => ['simple_read_more/main'],
        ],
      ];
    }

    return $elements;
  }

}
