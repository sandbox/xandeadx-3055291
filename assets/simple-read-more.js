(function ($, Drupal) {
  Drupal.behaviors.simpleReadMore = {
    attach: function (context, settings) {
      $('.collapsible-text__link', context).once('collapsible-text').on('click', function (event) {
        var $moreLink = $(this);
        var $container = $moreLink.parents('.collapsible-text');

        if (!$container.hasClass('collapsible-text--open')) {
          $container.addClass('collapsible-text--open');

          $moreLink.data('html', $moreLink.html());
          $moreLink.html(Drupal.t('Collapse'));
        }
        else {
          $container.removeClass('collapsible-text--open');
          $moreLink.html($moreLink.data('html'));
        }

        event.preventDefault();
      });
    }
  };
})(jQuery, Drupal);
